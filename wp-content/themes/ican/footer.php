<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Scriptable
 * @since Scriptable 1.0
 */
?>
<div class="footer-main">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-4">
                <div class="footer-main__content">
                    <span class="block-title">
                        Name here
                    </span>
                    <div class="footer-main__content__item">
                        <i class="fa fa-phone" aria-hidden="true"></i>
                        <a href="tel:0161279422">0161279422</a>
                    </div>
                    <div class="footer-main__content__item">
                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                        <a href="mailto:hello@yourmail.com">hello@yourmail.com</a>
                    </div>
                </div>
                <div class="footer-main__social">
                    <ul class="footer-main__social__links">
                        <li class="footer-main__social__links__item">
                            <a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li class="footer-main__social__links__item">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li class="footer-main__social__links__item">
                            <a href="#"><i class="fa fa-instagram"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-12 col-sm-2">
                <span class="block-title">Name here</span>
                <div class="footer-main__links">
                    <ul class="footer-main__links__holder">
                        <li class="footer-main__links__holder__item">
                            <a href="#">About Us</a>
                        </li>
                        <li class="footer-main__links__holder__item">
                            <a href="#">Blog</a>
                        </li>
                        <li class="footer-main__links__holder__item">
                            <a href="#">Jobs</a>
                        </li>
                        <li class="footer-main__links__holder__item">
                            <a href="#">Contact Us</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-12 col-sm-2">
                <span class="block-title">Name here</span>
                <div class="footer-main__links">
                    <ul class="footer-main__links__holder">
                        <li class="footer-main__links__holder__item">
                            <a href="#">About Us</a>
                        </li>
                        <li class="footer-main__links__holder__item">
                            <a href="#">Blog</a>
                        </li>
                        <li class="footer-main__links__holder__item">
                            <a href="#">Jobs</a>
                        </li>
                        <li class="footer-main__links__holder__item">
                            <a href="#">Contact Us</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <span class="block-title">Name here</span>
                <div class="footer-main__maps">
                    <img class="img-responsive" src="<?php bloginfo('template_directory'); ?>/images/home1_map.png">
                </div>
            </div>
            <div class="col-xs-12">
                <div class="footer-main__copyright">
                    <p>Designed by <a href="#">Dayfive Digital</a></p>
                    <ul class="footer-main__copyright__links">
                        <li class="footer-main__copyright__links__item">
                            <a href="#">Privacy</a>
                        </li>
                        <li class="footer-main__copyright__links__item">
                            <a href="#">Terms</a>
                        </li>
                        <li class="footer-main__copyright__links__item">
                            <a href="#">Cookies</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    jQuery(window).scroll(function() {
// 100 = The point you would like to fade the nav in.

        if (jQuery(window).scrollTop() > 100 ){

            jQuery('.bg').addClass('bg-color');

        } else {

            jQuery('.bg').removeClass('bg-color');

        };
    });

    jQuery('.scroll').on('click', function(e){
        e.preventDefault()

        jQuery('html, body').animate({
            scrollTop : jQuery(this.hash).offset().top
        }, 1500);
    });

    // fire default bpHS!
    jQuery('#demo-default').bpHS({
        nextText : '<i class="fa fa-angle-right"></i>',
        prevText : '<i class="fa fa-angle-left"></i>',
        autoPlay: true
    });

</script>
<script>
    jQuery('.bxslider').bxSlider({
        auto: true,
        minSlides: 3,
        maxSlides: 3,
        slideWidth: 250,
        slideMargin: 10,
    });
</script>

<?php wp_footer(); ?>
</div> <!-- closing row -->
</div> <!-- closing container-fluid -->
</body>
</html>