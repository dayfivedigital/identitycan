<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Scriptable
 * @since Scriptable 1.0
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title><?php wp_title(); ?></title>
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <?php wp_head(); ?>
</head>

<body id="page1" <?php body_class(); ?>>

<header class="navbar navbar-default navbar-fixed-top bg">
   <div class="container">
       <div class="row">
           <div class="col-xs-10 col-md-2 col-md-offset-1 col-sm-2">
               <div class="logo">
                   <a href="<?php echo get_home_url(); ?>">
                       <img src="<?php echo get_template_directory_uri(); ?>/images/icon-logo.png" alt="day five logo" class="img-responsive black">
                       <img src="<?php echo get_template_directory_uri(); ?>/images/icon-logo-white.png" alt="day five logo" class="img-responsive white">
                   </a>
               </div>
           </div>
           <div class="col-xs-2 hidden-lg hidden-md hidden-sm">
               <div class="navbar-header">
                   <div class="row">
                       <div class="col-xs-12 menu-toggle">
                           <div class="row">
                               <button type="button" class="navbar-toggle collapsed" id="nav1-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                   <span class="sr-only">Toggle navigation</span>
                                   <i class="fa fa-bars"></i>
                                   <a class="navbar-brand hidden" href="#">Menu</a>
                               </button>
                           </div>
                           <div class="collapse navbar-collapse hidden-lg hidden-md hidden-sm" id="search">
                               <form class="navbar-form navbar-left" role="search">
                                   <div class="form-group">
                                       <input type="text" class="form-control" placeholder="Search">
                                   </div>
                                   <button type="submit" id="submit-toggle" class="btn btn-default">Submit</button>
                               </form>
                           </div>
                       </div>

                   </div>
               </div>
           </div>
           <nav class="col-md-8 col-sm-10 col-xs-12">
               <div class="row">
                   <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                       <?php /* Primary navigation */
                       wp_nav_menu( array(
                               'menu' => 'top_menu',
                               'depth' => 2,
                               'container' => false,
                               'menu_class' => 'nav',
                               //Process nav menu using our custom nav walker
                               'walker' => new wp_bootstrap_navwalker())
                       );
                       ?>
                   </div>
               </div>
           </nav>
       </div>
   </div>
</header>

<div class="container-fluid">
    <div class="row">







