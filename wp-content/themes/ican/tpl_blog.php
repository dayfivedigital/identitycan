<?php
/**
 * template name: blog posts
 */
?>
<?php get_header() ?>

<?php global $post; ?>
<?php
$src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' );
?>
<div class="default-holder-block default-holder-block--blog" style="background-image: url(<?php echo $src[0]; ?> )">
    <div class="default-holder-block__overlay"></div>
    <div class="default-holder-block__absolute-content">
        <div class="col-xs-12">
            <h1 class="default-holder-block__absolute-content__title">
                <?php
                if ( is_single() ) :
                    the_title( '<h1 class="entry-title">', '</h1>' );
                else :
                    the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
                endif;
                ?>
            </h1>
        </div>
    </div>
</div>

<div class="blog-holder">
    <div class="container">
        <div class="row">
            <h3>News feed</h3>
        </div>
    </div>
</div>

<div class="blog-content">
    <div class="blog-content__cats">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <?php wp_list_categories('orderby=name&show_count=1&exclude=10'); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="blog-content__list">
        <div class="container">
            <div class="row">

                <div class="col-xs-12">
                    <span class="blog-content__list__month"><?php the_time('M Y'); ?></span>
                </div>

                <?php $posts_per_page = 5;
                query_posts('post_type=post&post_status=publish&posts_per_page='.$posts_per_page.'&paged='. get_query_var('paged').'&orderby=month&order=DESC'); ?>

                <?php if( have_posts() ): ?>

                <?php $i = 0; ?>
                <div class="col-xs-12">

                    <?php while( have_posts() ): the_post(); $i++; ?>

                        <div class="blog-content__list__blog-main">
                            <div id="post-<?php echo get_the_ID(); ?>" <?php post_class(); ?>>
                                <div class="blog-content__list__blog-main__img-block">
                                    <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( array(579, 579)); ?></a>
                                </div>
                                <div class="blog-content__list__blog-main__post-content">
                                    <h2 class="blog-content__list__blog-main__post-content__feed-title" ><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

                                    <div class="blog-content__list__blog-main__post-content__post-detail-block">
                                        <p class="blog-content__list__blog-main__post-content__post-detail-block__time" > <?php the_time('M j, Y'); ?> </p>
                                        <?php the_excerpt(__('Continue reading »','example')); ?>
                                    </div>

                                    <a href="<?php the_permalink(); ?>" class="blog-content__list__blog-main__post-content__anchor">Read More</a>

                                    <ul class="blog-content__list__blog-main__post-content__btn-social">
                                        <li>
                                            <a href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&amp;t=<?php the_title(); ?>"  target="_blank" class="btn-social-icon btn-social-icon--fb" title="<?php the_title(); ?> Share on Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                        </li>
                                        <li>
                                            <a href="http://twitter.com/home/?status=<?php the_title(); ?> - <?php the_permalink(); ?>" target="_blank" class="btn-social-icon btn-social-icon--tw" title="<?php the_title(); ?> Share on Twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                        </li>
                                    </ul>

                                </div>
                            </div><!-- /#post-<?php get_the_ID(); ?> -->
                        </div>
                        <?php if($i % 6 == 0){
                            echo '</div><div class="col-lg-12 col-sm-12 mas-grid load-track">'; $i = 0;
                        } ?>

                    <?php endwhile; ?>

                    <?php else: ?>

                        <div id="post-404" class="noposts">

                            <p><?php _e('None found.','example'); ?></p>

                        </div>

                    <?php endif; wp_reset_query(); ?>

                </div>
                <div class="col-xs-12">
                    <?php $count_posts = wp_count_posts(); ?>
                    <?php if ($count_posts > 5): ?>
                        <p id="load-posts" paged="<?php echo get_query_var('paged'); ?>">See More Posts</p>
                    <?php else: ?>
                        <p id="load-posts" class="no-more-post-button" paged="<?php echo get_query_var('paged'); ?>">No More Posts</p>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>
