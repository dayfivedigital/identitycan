<?php
/*
Template Name: Contact
*/

?>

<?php get_header() ?>

<?php global $post; ?>
<?php
$src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' );
?>
<div class="default-holder-block" style="background-image: url(<?php echo $src[0]; ?> )">
    <div class="default-holder-block__overlay"></div>
    <div class="default-holder-block__absolute-content">
        <div class="col-xs-12">
            <h1 class="default-holder-block__absolute-content__title">
                <?php
                if ( is_single() ) :
                    the_title( '<h1 class="entry-title">', '</h1>' );
                else :
                    the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
                endif;
                ?>
            </h1>
        </div>
    </div>
</div>



<div class="bg-grey">
    <div class="container">
        <div class="row">
            <div class="contact-holder">
                <div class="col-xs-12">
                    <div class="contact-holder__content">
                        <h2>CONTACT US</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="contact-holder__form">
                        <?php echo do_shortcode( '[contact-form-7 id="48" title="Contact form 1"]' ) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-xs-12 col-sm-4">
    <div class="row mobile-row">
        <div class="contact-information-block">
            <div class="contact-information-block__text">
                <p>Office G17 <br>
                    Technology House <br>
                    2 Lissadel Street <br>
                    Salford, M6 6AP</p>
            </div>
        </div>
    </div>
</div>
<div class="col-xs-12 col-sm-4">
    <div class="row mobile-row">
        <div class="contact-information-block">
            <div class="contact-information-block__text contact-information-block--second">
                <a href="#" target="_blank">
                    <i class="fa fa-twitter"></i>
                    Identitycan on Twitter
                </a>
            </div>
        </div>
    </div>
</div>
<div class="col-xs-12 col-sm-4">
    <div class="row">
        <div class="contact-information-block">
            <div class="contact-information-block__text">
                <a href="#" target="_blank">
                    <i class="fa fa-facebook"></i>
                    Identitycan on Facebook
                </a>
            </div>
        </div>
    </div>
</div>



<?php get_footer() ?>