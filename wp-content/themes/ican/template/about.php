<?php
/*
Template Name: About
*/

?>

<?php get_header() ?>

<?php global $post; ?>
<?php
$src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' );
?>
<div class="default-holder-block" style="background-image: url(<?php echo $src[0]; ?> )">
    <div class="default-holder-block__overlay"></div>
    <div class="default-holder-block__absolute-content">
        <div class="col-xs-12">
            <h2 class="default-holder-block__absolute-content__title">
                <?php the_title(); ?>
            </h2>
        </div>
    </div>
</div>

<div class="about-content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="about-content__intro">
                    <h2>ABOUT US</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <img class="img-responsive" src="http://placehold.it/800x471">
            </div>
            <div class="col-xs-12 col-sm-8">
                <div class="about-content__main">
                    <h2>Who Are We</h2>
                    <p>Identity, Connexions and Network is a charity committed to improving the lives of Africans and immigrants in Leicester.
                        The charity was born from the recognition that immigrants, particularly those from African backgrounds, had limited access to vital support and services.
                        We aim to empower people and communities by providing them with the necessary skills, tools, and support through our many activities and services.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="home-team">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="home-team__content">
                    <h2>Our Team</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing repudiandae illo natus</p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="thumbnail">
                    <img src="http://placehold.it/250x250" alt="place it" class="img-circle img-responsive">
                    <div class="caption">
                        <h3>Thumbnail label</h3>
                        <p class="member-description">
                            Lorem ipsum dolor sit amet, consectetur adipisicing repudiandae illo natus
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="thumbnail">
                    <img src="http://placehold.it/250x250" alt="place it" class="img-circle img-responsive">
                    <div class="caption">
                        <h3>Thumbnail label</h3>
                        <p class="member-description">
                            Lorem ipsum dolor sit amet, consectetur adipisicing repudiandae illo natus
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="thumbnail">
                    <img src="http://placehold.it/250x250" alt="place it" class="img-circle img-responsive">
                    <div class="caption">
                        <h3>Thumbnail label</h3>
                        <p class="member-description">
                            Lorem ipsum dolor sit amet, consectetur adipisicing repudiandae illo natus
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="thumbnail">
                    <img src="http://placehold.it/250x250" alt="place it" class="img-circle img-responsive">
                    <div class="caption">
                        <h3>Thumbnail label</h3>
                        <p class="member-description">
                            Lorem ipsum dolor sit amet, consectetur adipisicing repudiandae illo natus
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer() ?>