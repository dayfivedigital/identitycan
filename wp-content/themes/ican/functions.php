<?php
/* Theme setup */
add_action( 'after_setup_theme', 'wpt_setup' );
if ( ! function_exists( 'wpt_setup' ) ):
    function wpt_setup() {
        register_nav_menu( 'primary', __( 'Primary navigation', 'wptuts' ) );
    } endif;
?>
<?php // Register custom navigation walker
require_once('wp_bootstrap_navwalker.php');
?>
<?php
/**
 * Scriptable functions and definitions
 *
 * @package WordPress
 * @subpackage Scriptable
 * @since Scriptable 1.0
 */

//Include Stylesheet

if ( ! is_admin() ) {
    wp_enqueue_style('scriptable', get_template_directory_uri() . '/style.css');

    //Include Jquery
    wp_enqueue_script('jquery');

    //Include Bootstrap JS
    wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js');

    wp_enqueue_script('bxslider', get_template_directory_uri() . '/js/jquery.bxslider.min.js');

    wp_enqueue_script('bpHS', get_template_directory_uri() . '/js/bpHS.min.js');

    //Include Font Awesome
    wp_enqueue_style( 'font-awesome',"//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css",'','4.2.0');

    //Include Google fonts Roboto
    wp_enqueue_style('google-fonts', "//fonts.googleapis.com/css?family=Roboto");
}

//Add WP Menu Support

function register_my_menu() {
    register_nav_menu('header-menu',__( 'Header Menu' ));
}
add_action( 'init', 'register_my_menu' );

//Add Thumbnail Support

add_theme_support( 'post-thumbnails' );

//Add Portfolio Custom Post Type

add_action('init', 'portfolio_register');

function portfolio_register() {
    $args = array(
        'label' => __('Portfolio'),
        'singular_label' => __('Project'),
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => true,
        'supports' => array('title', 'editor', 'thumbnail')
    );

    register_post_type( 'portfolio' , $args );
}


//Add Gallery Custom Post Type

$gallery_labels = array(
    'name' => _x('Client Logos', 'post type general name'),
    'singular_name' => _x('Client Logos', 'post type singular name'),
    'add_new' => _x('Add New', 'gallery'),
    'add_new_item' => __("Add New Gallery"),
    'edit_item' => __("Edit Gallery"),
    'new_item' => __("New Gallery"),
    'view_item' => __("View Gallery"),
    'search_items' => __("Search Gallery"),
    'not_found' =>  __('No galleries found'),
    'not_found_in_trash' => __('No galleries found in Trash'),
    'parent_item_colon' => ''

);

$gallery_args = array(
    'labels' => $gallery_labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => true,
    'hierarchical' => false,
    'menu_position' => null,
    'capability_type' => 'post',
    'supports' => array('title', 'editor')
);

register_post_type('gallery', $gallery_args);


//Include WP Alchemy & Metabox

include_once get_template_directory() . '/metaboxes/wp-alchemy/metabox.php';
include_once get_template_directory() . '/metaboxes/content-block-spec.php';

//Add Content Blocks

require get_template_directory() . '/content-blocks.php';

//Homepage Metaboxes

$post_card_metabox = new WPAlchemy_MetaBox(array
(
    'id' => '_short_cardtag',
    'title' => 'Card Tag',
    'types' => array('post'),
    'template' => STYLESHEETPATH . '/metaboxes/simple-meta.php'
));

$post_des_metabox = new WPAlchemy_MetaBox(array
(
    'id' => '_short_des',
    'title' => 'Short Description',
    'types' => array('post'),
    'template' => STYLESHEETPATH . '/metaboxes/simple-meta.php'
));
