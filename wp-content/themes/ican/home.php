<?php
/*
Template Name: Home
*/
?>
<?php get_header(); ?>
<div class="bp-hs" id="demo-default">
    <div class="bp-hs_inner">
        <div class="bp-hs_inner__item" data-transform="scale">
            <img src="<?php bloginfo('template_directory'); ?>/images/meeting.jpg" class="img-responsive" alt="Boompx Hero Slider 01"/>
        </div>
    </div>
</div>
<div class="logo-holder">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="logo-holder__carousel-wrap carousel-wrap">
                    <ul class="logo-holder__carousel-wrap__products-grid bxslider">
                        <li class="logo__carousel-wrap__products-grid__item">
                            <img src="<?php bloginfo('template_directory'); ?>/images/lottery.png" class="img-responsive" alt="manchester skyline" />
                        </li>
                        <li class="logo__carousel-wrap__products-grid__item">
                            <img src="<?php bloginfo('template_directory'); ?>/images/lottery.png" class="img-responsive" alt="manchester skyline" />
                        </li>
                        <li class="logo__carousel-wrap__products-grid__item">
                            <img src="<?php bloginfo('template_directory'); ?>/images/lottery.png" class="img-responsive" alt="manchester skyline" />
                        </li>
                        <li class="logo__carousel-wrap__products-grid__item">
                            <img src="<?php bloginfo('template_directory'); ?>/images/lottery.png" class="img-responsive" alt="manchester skyline" />
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="home-intro">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h2>Our mission</h2>
                <p>We are charity/ non-profit/ fundraising/ NGO organizations. Our charity activities are taken place around the world,let contribute to them with us by your hand to be a better life.</p>
            </div>
        </div>
    </div>
</div>
<div class="home-social">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="home-social__list">
                    <ul class="home-social__list__links">
                        <li class="home-social__list__links__item">
                            <a href="#" target="_blank">
                                <i class="fa fa-twitter"></i>
                                Follow us on Twitter
                            </a>
                        </li>
                        <li class="home-social__list__links__item">
                            <a href="#" target="_blank">
                                <i class="fa fa-facebook"></i>
                                Join us on Facebook
                            </a>
                        </li>
                        <li class="home-social__list__links__item">
                            <a href="#" target="_blank">
                                <i class="fa fa-envelope-o"></i>
                                Send us an email
                            </a>
                        </li>
                        <li class="home-social__list__links__item">
                            <a href="#"></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="home-more-content">
    <div class="col-xs-12 col-sm-4">
        <div class="row">
            <div class="home-more-content--pink">
                <div class="col-xs-12">
                    <h2>About Us</h2>
                    <p>Identity, Connexions and Network is a charity committed to improving the lives of Africans and immigrants in Leicester.</p>
                    <p>The charity was born from the recognition that immigrants, particularly those from African backgrounds, had limited access to vital support and services.</p>
                    <a href="#">Read More</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-4">
        <div class="row">
            <div class="home-more-content--blue">
              <div class="col-xs-12">
                  <h2>Services</h2>
                  <p>We offer a range of services and activities to meet the needs of our community and service users. We offer the following:</p>
                  <p>The charity was born from the recognition that immigrants, particularly those from African backgrounds, had limited access to vital support and services.</p>
                  <a href="#">Read More</a>
              </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-4">
        <div class="row">
            <div class="home-more-content--orange">
                <div class="col-xs-12">
                    <h2>Something</h2>
                    <p>Identity, Connexions and Network is a charity committed to improving the lives of Africans and immigrants in Leicester.</p>
                    <p>The charity was born from the recognition that immigrants, particularly those from African backgrounds, had limited access to vital support and services.</p>
                    <a href="#">Read More</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="home-latest">
<div class="container">
<div class="row">
            <div class="col-xs-12">
                <div class="home-latest__main-content">
                    <h2>OUR BLOG</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                </div>
            </div>
            <?php $posts_per_page = 3; query_posts('post_type=post&post_status=publish&posts_per_page='.$posts_per_page.'&paged='. get_query_var('paged').'&orderby=date&order=DESC'); ?>

            <?php if( have_posts() ): ?>


                <?php while( have_posts() ): the_post(); ?>
                    <div class="col-xs-12 col-sm-4">
                        <div class="home-latest__card-holders">
                            <a href="<?php the_permalink(); ?>">
                                <?php global $post; ?>
                                <?php
                                $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' );
                                ?>
                                <div class="home-latest__card-holders__bg-img" style="background-image: url(<?php echo $src[0]; ?> )"></div>
                                <div class="home-latest__card-holders__card">
                                        <h3><?php the_title(); ?></h3>
                                    <div class="home-latest__card-holders__card__post-meta">
                                        <span><i class="fa fa-calendar" aria-hidden="true"></i><?php the_time('M j, Y'); ?></span>
                                    </div>
                                    <?php the_excerpt(__('Continue reading »','example')); ?>
                                </div>
                            </a>
                        </div>
                    </div>
                <?php endwhile; ?>


            <?php else: ?>
                <div id="post-404" class="noposts">
                    <p><?php _e('None found.','example'); ?></p>
                </div>
            <?php endif; wp_reset_query(); ?>

</div>
</div>
</div>
<div class="before-cta">
    <div class="container">
        <div class="row">
            <div class="before-cta__content">
                <div class="col-xs-12 col-sm-9">
                    <p>There are 9.2 million kid in Africa who suffers from starvation who need our help</p>
                </div>
                <div class="col-xs-12 col-sm-3">
                    <a href="#">Become a member</a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>
