<?php
/**
 * Single Post Template: [Descriptive Template Name]
 * Template for displaying all single posts.
 */

get_header();

?>


    <div id="main" class="single-post">
        <?php

        while ( have_posts() ) : the_post();

            get_template_part( 'content', get_post_format() );

        endwhile; ?>
    </div>

    <div class="single-footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-4">
                    <div class="single-footer__holder">
                        <h2>Share this page</h2>
                        <ul class="single-footer__holder__btn-social">
                            <li>
                                <a href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&amp;t=<?php the_title(); ?>"  target="_blank" class="btn-social-icon btn-social-icon--fb" title="<?php the_title(); ?> Share on Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            </li>
                            <li>
                                <a href="http://twitter.com/home/?status=<?php the_title(); ?> - <?php the_permalink(); ?>" target="_blank" class="btn-social-icon btn-social-icon--tw" title="<?php the_title(); ?> Share on Twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <?php $posts_per_page = 2;
                query_posts('post_type=post&post_status=publish&posts_per_page='.$posts_per_page.'&paged='. get_query_var('paged').'&orderby=month&order=DESC'); ?>

                <?php if( have_posts() ): ?>

                <?php $i = 0; ?>
                <div class="single-post-holder-block col-xs-12 col-sm-8">
                    <h2 class="single-post-holder-block__title">Related Content</h2>
                    <?php while( have_posts() ): the_post(); $i++; ?>

                        <div class="single-post-holder-block__blog-main col-xs-12 col-sm-6">
                           <div class="row">
                               <div id="post-<?php echo get_the_ID(); ?>" <?php post_class(); ?>>
                                   <div class="single-post-holder-block__blog-main__img-block">
                                       <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( array(579, 579)); ?></a>
                                   </div>
                                   <div class="single-post-holder-block__blog-main__post-content">
                                       <h2 class="single-post-holder-block__blog-main__post-content__feed-title" ><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

                                       <a href="<?php the_permalink(); ?>" class="single-post-holder-block__blog-main__post-content__anchor">Read More</a>

                                   </div>
                               </div><!-- /#post-<?php get_the_ID(); ?> -->
                           </div>
                        </div>
                        <?php if($i % 6 == 0){
                            echo '</div><div class="col-lg-12 col-sm-12 mas-grid load-track">'; $i = 0;
                        } ?>

                    <?php endwhile; ?>

                    <?php else: ?>

                        <div id="post-404" class="noposts">

                            <p><?php _e('None found.','example'); ?></p>

                        </div>

                    <?php endif; wp_reset_query(); ?>

                </div>
            </div>
        </div>
    </div>

<?php get_footer(); ?>